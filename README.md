This project allows me to simulate various scenarios for Navistream using Vilmock, so far that includes toggling ECM, changing accel, rpm, ect, and simulating extended drives (by faking the GPS)

Read more about Xamarin UITest here: https://developer.xamarin.com/guides/testcloud/uitest/


##Prerequisites
- .NET 4.5
- VS 2015 or higher

##Running the Tests
- Ensure you have a UITest Vilmock APK available (see `uitest` branch of Vlimock), paste it's static path in `VilmockTest > Tests > APK_PATH` 
- Open the project have a target Android device available (emulators work fine), go to Test > Run > Run All

This will install the APK to the target device and run the selected tests.