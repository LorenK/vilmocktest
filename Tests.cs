﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;

namespace VilmockTest
{
    [TestFixture]
    public class Tests
    {
        private const string APK_PATH = @"D:\Documents\work\navistream-vehicleclient\java\vilmock\build\outputs\apk\vilmock-debug.apk";
        private AndroidApp app;
        
        [SetUp]
        public void BeforeEachTest()
        {
            app = ConfigureApp
                .Android
                .ApkFile(APK_PATH)
                .StartApp();
        }
        
        [Test]
        public void REPL()
        {
            app.Repl();
        }

        // Result: App never went into driving mode
        [Test]
        public void DriveWithNoEngine()
        {
            // Vilmock gets launched

            // Disconnect ECM
            app.SetEcm(false);
            ThreadSleepers.WaitASec();
            app.LaunchNavistream();
            ThreadSleepers.Sleep(4.0);
            app.LaunchVilmock();

            // Connect ECM
            app.SetEcm(true);
            
            // Switch to Navistream
            app.LaunchNavistream();

            // Start moving GPS, 60km/h for 60 seconds
            app.Travel(seconds: 60);

            // Once travel is finished, disable ECM and shut down the engine
            app.LaunchVilmock();
            ThreadSleepers.WaitASec();
            app.SetEcm(false);
            
            // Launch Navistream
            app.LaunchNavistream();
        }

        // Result: RAM spikes, app goes into driving mode, after a while, RAM usage levels out and mode enters non-driving
        [Test]
        public void DriveWithEngine()
        {
            // Vilmock gets launched

            // Disconnect ECM
            app.SetEcm(false);
            ThreadSleepers.WaitASec();
            app.LaunchNavistream();
            ThreadSleepers.Sleep(4.0);
            app.LaunchVilmock();

            // Connect ECM
            app.SetEcm(true);

            // Start the engine (accelerate)
            app.StartEngine();

            // Give engine time to warm up
            ThreadSleepers.Sleep(5.0);

            // Switch to Navistream
            app.LaunchNavistream();

            // Start moving GPS, 60km/h for 5 mintes
            app.Travel(seconds: 300);

            // Once travel is finished, disable ECM and shut down the engine
            app.LaunchVilmock();
            ThreadSleepers.WaitASec();
            app.SetEcm(false);
            app.StopEngine();

            // Launch Navistream
            app.LaunchNavistream();
        }
        
        // Result: Pretty sure app crashed with enough distance traveling
        [Test]
        public void DriveWithEngineExtraLong()
        {
            // Vilmock gets launched

            // Disconnect ECM
            app.SetEcm(false);
            ThreadSleepers.WaitASec();
            app.LaunchNavistream();
            ThreadSleepers.Sleep(4.0);
            app.LaunchVilmock();

            // Connect ECM
            app.SetEcm(true);

            // Start the engine (accelerate)
            app.StartEngine();

            // Give engine time to warm up
            ThreadSleepers.Sleep(5.0);

            // Switch to Navistream
            app.LaunchNavistream();

            // Start moving GPS, this is a long journey
            app.Travel(seconds: 14400);

            // Once travel is finished, disable ECM and shut down the engine
            app.LaunchVilmock();
            ThreadSleepers.WaitASec();
            app.SetEcm(false);
            app.StopEngine();

            // Launch Navistream
            app.LaunchNavistream();
        }

        // Result: RAM usage remains mostly level until entering drive mode, once in drive mode, RAM gradually fills up, then is cleared, no RAM spiking since we're not travelling
        [Test]
        public void StartEngineNoDrive()
        {
            // Vilmock gets launched

            // Disconnect ECM
            app.SetEcm(false);
            ThreadSleepers.WaitASec();
            app.LaunchNavistream();
            ThreadSleepers.Sleep(4.0);
            app.LaunchVilmock();

            // Connect ECM
            app.SetEcm(true);

            // Start the engine (accelerate)
            app.StartEngine();

            // Give engine time to warm up
            ThreadSleepers.Sleep(5.0);

            // Switch to Navistream
            app.LaunchNavistream();

            ThreadSleepers.Sleep(2);

            // Once travel is finished, disable ECM and shut down the engine
            app.LaunchVilmock();
            ThreadSleepers.WaitASec();
            app.SetEcm(false);
            app.StopEngine();

            // Launch Navistream
            app.LaunchNavistream();
        }
    }
}