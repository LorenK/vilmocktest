﻿using System;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;
using System.Threading;

namespace VilmockTest
{
    public static class SeekBarExtension
    {

        /// <summary>
        /// Gets SeekBar elements
        /// </summary>
        /// <param name="id">ID to match</param>
        /// <returns></returns>
        public static AppQuery SeekBar(this AppQuery query, string id)
        {
            return query.Class("SeekBar").Id(id);
        }
    }

    public class SeekBar
    {
        private AndroidApp app { get; set; }
        private string id { get; set; }

        public SeekBar(AndroidApp app, string id)
        {
            this.app = app;
            this.id = id;
        }

        public double GetProgress()
        {
            return app.Query(a => a.SeekBar(id).Invoke("getProgress").Value<double>())[0];
        }

        public void SetProgress(int val)
        {
            app.Query(a => a.SeekBar(id).Invoke("setProgress", val));
        }
    }

    public static class ThreadSleepers
    {
        public static void Sleep(int minutes)
        {
            int wait = (minutes * 60000);
            Thread.Sleep(wait);
            Console.WriteLine(string.Format("Waited {0} minutes", wait));
        }

        public static void Sleep(double seconds)
        {
            int wait = (int)(seconds * 1000);
            Thread.Sleep(wait);
            Console.WriteLine(string.Format("Waited {0} seconds", wait));
        }

        public static void WaitASec()
        {
            Thread.Sleep(1000);
            Console.WriteLine("Waited 1000 ms");
        }
    }

    public static class AndroidAppExtensions
    {
        public static void LaunchVilmock(this AndroidApp app)
        {
            app.Invoke("LaunchVilmock");
            Console.WriteLine("Launched Vilmock");
        }

        public static void LaunchNavistream(this AndroidApp app)
        {
            app.Invoke("LaunchNavistream");
            Console.WriteLine("Launched Navistream");
        }

        public static void SetEcm(this AndroidApp app, bool ecmEnabled)
        {
            const string ecm = "statusSwitch";
            app.Query(a => a.Switch(ecm).Invoke("setChecked", ecmEnabled));
            app.Invoke("SetEcm", ecmEnabled);
            Console.WriteLine(ecmEnabled ? "Enabled ECM" : "Disabled ECM");
        }
        
        public static void StartEngine(this AndroidApp app)
        {
            const string accelerationSeekBarId = "accelerationSeekBar";
            const string fuelRateSeekBarId = "fuelRateSeekBar";
            const string boostSeekBarId = "boostSeekBar";
            const string rpmSeekBarId = "rpmSeekBar";
            const string engineLoadSeekBarId = "engineLoadSeekBar";

            const int val = 150;

            var accelSeekbar = new SeekBar(app, accelerationSeekBarId);
            accelSeekbar.SetProgress(110);

            var fuelRateSeekbar = new SeekBar(app, fuelRateSeekBarId);
            fuelRateSeekbar.SetProgress(val);

            var boostSeekBar = new SeekBar(app, boostSeekBarId);
            boostSeekBar.SetProgress(val);

            var rpmSeekBar = new SeekBar(app, rpmSeekBarId);
            rpmSeekBar.SetProgress(1000);

            var engineLoadSeekBar = new SeekBar(app, engineLoadSeekBarId);
            engineLoadSeekBar.SetProgress(val);

            var speedSeekBar = new SeekBar(app, "speedSeekBar");
            while(speedSeekBar.GetProgress() < 58)
            {
                Thread.Sleep(1000);
            }
            accelSeekbar.SetProgress(100);
        }

        public static void StopEngine(this AndroidApp app)
        {
            const string accelerationSeekBarId = "accelerationSeekBar";
            const string fuelRateSeekBarId = "fuelRateSeekBar";
            const string boostSeekBarId = "boostSeekBar";
            const string rpmSeekBarId = "rpmSeekBar";
            const string engineLoadSeekBarId = "engineLoadSeekBar";

            const int val = 0;

            var accelSeekbar = new SeekBar(app, accelerationSeekBarId);
            accelSeekbar.SetProgress(val);

            var fuelRateSeekbar = new SeekBar(app, fuelRateSeekBarId);
            fuelRateSeekbar.SetProgress(val);

            var boostSeekBar = new SeekBar(app, boostSeekBarId);
            boostSeekBar.SetProgress(val);

            var rpmSeekBar = new SeekBar(app, rpmSeekBarId);
            rpmSeekBar.SetProgress(val);

            var engineLoadSeekBar = new SeekBar(app, engineLoadSeekBarId);
            engineLoadSeekBar.SetProgress(val);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="mps">Meters per second, default is 16.6667, or ~60km/h</param>
        /// <param name="seconds">How long to travel in seconds, default is 30 seconds</param>
        public static void Travel(this AndroidApp app, double mps = 16.6667, int seconds = 30, int delay = 1000)
        {
            var baseCoords = new double[] { 37.31917, -122.04511 };

            var kph = mps * 3.6;
            Console.WriteLine(string.Format("Moving at {0}km/h", kph));

            double lat = baseCoords[0];

            for (int i = 0; i < seconds; i++)
            {
                lat += mps * 0.000008993;
                double lng = baseCoords[1];
                app.Device.SetLocation(lat, lng);
                Console.WriteLine(string.Format("Set location to {0}, {1}", lat, lng));
                Thread.Sleep(delay);
            }

            Console.WriteLine("Finished traveling");
        }

        public static void TravelRandomly(this AndroidApp app, int seconds = 30)
        {
            double mps = 16.6667;
            int delay = 1000;

            var baseCoords = new double[] { 37.31917, -122.04511 };
            
            double lat = baseCoords[0];

            for (int i = 0; i < seconds; i++)
            {
                mps = new Random().Next(16, 25);
                delay = new Random().Next(500, 5000);
                
                Console.WriteLine(string.Format("Moving at {0}m/{1}s", mps, (double)delay/1000));

                lat += mps * 0.000008993;
                double lng = baseCoords[1];
                app.Device.SetLocation(lat, lng);
                Console.WriteLine(string.Format("Set location to {0}, {1}", lat, lng));
                Thread.Sleep(delay);
            }

            Console.WriteLine("Finished traveling");
        }
    }
}
